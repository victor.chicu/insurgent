package service.insurgent;

import io.netty.channel.ChannelHandlerContext;
import messages.Token;
import service.IInsurgent;

/**
 * Created by Victor on 25.06.2017.
 */
public class Insurgent implements IInsurgent {
    private final ChannelHandlerContext context;

    public Insurgent(ChannelHandlerContext context) {
        this.context = context;
    }

    @Override
    public void logout() {

    }

    @Override
    public void signUp(String email, String password) {

    }

    @Override
    public void sendMessage(String to, String message) {
        System.out.print("OK..");
    }

    @Override
    public Token signIn(String email, String password) {
        return null;
    }
}
