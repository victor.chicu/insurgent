package service.insurgent.invocations;

import service.insurgent.Insurgent;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Created by Victor on 25.06.2017.
 */
public class InvokableMethods {
    //-----

    private Insurgent insurgent;

    private HashMap<String, Method> invokableMethodsCache;

    private static InvokableMethods instance;

    //-----

    public InvokableMethods() {
        invokableMethodsCache = new HashMap<>();
    }

    //-----

    public void setInsurgent(Insurgent insurgent) {
        this.insurgent = insurgent;
    }

    public void cacheInvokableMethods() throws IllegalAccessException, InstantiationException {
        for (Method method : insurgent.getClass().getDeclaredMethods()) {
            invokableMethodsCache.put(method.getName(), method);
        }
    }

    public Method getMethodByName(String method) {
        Method result = invokableMethodsCache.get(method);
        return result;
    }

    public Insurgent newInstance() throws IllegalAccessException, InstantiationException {
        return Insurgent.class.newInstance();
    }

    public Insurgent getInsurgent() {
        return insurgent;
    }

    public static InvokableMethods getInstance() {
        if (instance == null) {
            instance = new InvokableMethods();
        }

        return instance;
    }


    //-----
}
