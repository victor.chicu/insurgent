package service.insurgent.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import service.insurgent.invocations.InvokableMethods;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor on 25.06.2017.
 */
public class PacketHandler {
    private final ByteBuf byteBuf;

    public PacketHandler(ByteBuf byteBuf) {
        this.byteBuf = byteBuf;
    }

    public void invokeMethod(ChannelHandlerContext context) {
        List<Object> arguments = new ArrayList<>();

        Method method = getMethod();

        Class<?>[] types = method.getParameterTypes();

        for (Class<?> type : types) {
            Integer size = byteBuf.readInt();
            byte[] buffer = new byte[size];
            byteBuf.readBytes(size).getBytes(size, buffer);
            String typeName = type.getTypeName();
        }

        try {
            Class<? extends Method> clazz = method.getClass();
            method.invoke(clazz, arguments);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private Method getMethod() {
        Integer size = byteBuf.readInt();
        byte[] buffer = new byte[size];
        byteBuf.readBytes(size).getBytes(size, buffer);
        String methodName = String.valueOf(buffer);
        return InvokableMethods.getInstance().getMethodByName(methodName);
    }
}
