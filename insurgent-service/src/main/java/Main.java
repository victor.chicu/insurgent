import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import service.netty.NettyService;

/**
 * Created by Victor on 17.06.2017.
 */
public class Main {
    private static Logger _logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();

        _logger.setLevel(Level.ALL);

        try {
            NettyService.start();
        } catch (Throwable e) {
            _logger.fatal(e.toString());
        }
    }
}