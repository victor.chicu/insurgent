package service;

import messages.Token;

/**
 * Created by Victor on 25.06.2017.
 */
public interface IInsurgent {
    void logout();

    void signUp(String email, String password);

    void sendMessage(String to, String message);

    Token signIn(String email, String password);
}