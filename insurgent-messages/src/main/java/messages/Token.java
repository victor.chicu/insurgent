package messages;

/**
 * Created by Victor on 03.07.2017.
 */
public class Token {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
