import client.insurgent.Insurgent;
import client.insurgent.managers.InsurgentManager;
import messages.Token;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by Victor on 25.06.2017.
 */
public class InsurgentTests {
    private String DEFAULT_HOST = "192.168.0.51";
    private Integer DEFAULT_PORT = 8992;

    private static Logger logger = Logger.getLogger(InsurgentTests.class);

    @BeforeClass
    public static void atTestStartUp() {

    }

    @AfterClass
    public static void atTestShutdown() {

    }

    @Test
    public void signIn() throws Exception {
        InsurgentManager insurgentManager = new InsurgentManager();

        insurgentManager.signUp("victor.chicu@hotmail.com", "ZAQ!2wsx");

        Insurgent insurgent = new Insurgent(DEFAULT_HOST, DEFAULT_PORT);

        Token token = insurgent.signIn("victor.chicu@hotmail.com", "ZAQ!2wsx");


    }

    @Test
    public void signUp() throws Exception {
        Insurgent insurgent = new Insurgent(DEFAULT_HOST, DEFAULT_PORT);

        insurgent.signUp("victor.chicu@hotmail.com", "ZAQ!2wsx!");
    }

    @Test
    public void sendMessage() throws Exception {
        Insurgent insurgent = new Insurgent(DEFAULT_HOST, DEFAULT_PORT);
        insurgent.sendMessage(UUID.randomUUID().toString(), "sendMessage");
        Thread.sleep(10000);
        System.out.print("OK..");
    }
}
