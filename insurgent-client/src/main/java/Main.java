import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Created by Victor on 17.06.2017.
 */
public class Main {
    private static Logger logger = Logger.getLogger(Main.class);

    //todo: Implement netty inside network library.

    public static void main(String[] args) {
        BasicConfigurator.configure();
        logger.setLevel(Level.ALL);
    }
}