package client.insurgent;

import client.insurgent.builders.PacketBuilder;
import client.netty.NettyClient;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import messages.Token;
import org.apache.log4j.Logger;
import service.IInsurgent;

/**
 * Created by Victor on 25.06.2017.
 */
public class Insurgent implements IInsurgent {
    //-----

    private final NettyClient nettyClient;

    private String tokenId;

    private static Logger logger = Logger.getLogger(Insurgent.class);

    //-----

    public Insurgent(String host, Integer port) throws Exception {
        nettyClient = new NettyClient(host, port);
        //authenticate
        nettyClient.connect();
    }

    //-----

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    //-----

    @Override
    public void logout() {

    }

    @Override
    public void signUp(String email, String password) {
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        PacketBuilder packetBuilder = new PacketBuilder();
        byte[] buffer = packetBuilder.addParameter(methodName).addParameter(email).addParameter(password).build();
        Channel channel = nettyClient.getChannel();
        ByteBuf byteBuf = channel.alloc().directBuffer(buffer.length).writeBytes(buffer);
        channel.writeAndFlush(byteBuf);
    }

    @Override
    public void sendMessage(String to, String message) {
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        PacketBuilder packetBuilder = new PacketBuilder();
        byte[] buffer = packetBuilder.addParameter(methodName).addParameter(to).addParameter(message).build();
        Channel channel = nettyClient.getChannel();
        ByteBuf byteBuf = channel.alloc().directBuffer(buffer.length).writeBytes(buffer);
        channel.writeAndFlush(byteBuf);
    }

    @Override
    public Token signIn(String email, String password) {
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        PacketBuilder packetBuilder = new PacketBuilder();
        byte[] buffer = packetBuilder.addParameter(methodName).addParameter(email).addParameter(password).build();
        Channel channel = nettyClient.getChannel();
        ByteBuf byteBuf = channel.alloc().directBuffer(buffer.length).writeBytes(buffer);
        channel.writeAndFlush(byteBuf);
        return new Token();
    }

    //-----
}