package client.insurgent.builders;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;

import java.util.*;

/**
 * Created by Victor on 25.06.2017.
 */
public class PacketBuilder {
    //-----

    private final ArrayList<byte[]> buffer;

    //-----

    public PacketBuilder() {
        buffer = new ArrayList();
    }

    public PacketBuilder(Collection<? extends byte[]> collection) {
        buffer = new ArrayList<>(collection);
    }

    //-----

    public byte[] build() {
        List<Byte> result = new ArrayList<>();

        for (byte[] byteList : buffer) {
            result.addAll(Bytes.asList(byteList));
        }

        return Bytes.toArray(result);
    }

    public PacketBuilder addParameter(String value) {
        byte[] bytes = value.getBytes();
        this.buffer.add(Ints.toByteArray(bytes.length));
        this.buffer.add(value.getBytes());
        return this;
    }

    //-----
}