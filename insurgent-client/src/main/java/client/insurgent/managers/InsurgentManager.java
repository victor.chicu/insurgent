package client.insurgent.managers;

import client.insurgent.Insurgent;
import io.netty.util.internal.StringUtil;

/**
 * Created by Victor on 03.07.2017.
 */
public class InsurgentManager {
    private final Insurgent insurgent;

    public InsurgentManager() throws Exception {
        insurgent = new Insurgent("127.0.0.1", 8992);
    }

    public void logout() {
        insurgent.logout();
    }

    public void signIn(String email, String password) throws Exception {
        if (StringUtil.isNullOrEmpty(email)) {
            throw new Exception("Email is null or empty.");
        }

        if (StringUtil.isNullOrEmpty(password)) {
            throw new Exception("Password is null or empty");
        }

        insurgent.signIn(email, password);
    }

    public void signUp(String email, String password) throws Exception {
        if (StringUtil.isNullOrEmpty(email)) {
            throw new Exception("Email is null or empty.");
        }

        if (StringUtil.isNullOrEmpty(password)) {
            throw new Exception("Password is null or empty");
        }

        insurgent.signUp(email, password);
    }
}
