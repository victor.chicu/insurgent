package client.netty.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by Victor on 18.06.2017.
 */
public final class NettyClientHandler extends SimpleChannelInboundHandler<Object> {
    private ChannelHandlerContext ctx;

    @Override
    public void channelRead0(ChannelHandlerContext ctx, Object entity) throws Exception {
        System.out.print("channelRead0");
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        this.ctx = ctx;
        System.out.println("ChannelActive");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        System.out.println("ChannelInactive");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
        System.out.println(cause.toString());
    }

    public ChannelHandlerContext getContext() {
        return ctx;
    }
}
