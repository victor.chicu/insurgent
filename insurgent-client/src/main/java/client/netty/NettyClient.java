package client.netty;

import client.insurgent.Insurgent;
import client.netty.handlers.NettyClientHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by Victor on 18.06.2017.
 */
public final class NettyClient implements Closeable {
    //-----

    private Channel channel;

    private final String host;
    private final Integer port;
    private final EventLoopGroup group;

    private static Logger logger = Logger.getLogger(Insurgent.class);

    //-----

    public NettyClient(String host, Integer port) {
        this.host = System.getProperty("host", host);
        this.port = port;
        group = new NioEventLoopGroup();
    }

    //-----

    @Override
    public void close() throws IOException {
        try {
            this.channel.close().sync();
        } catch (InterruptedException e) {
            logger.error(e.toString());
        } finally {
            group.shutdownGracefully();
        }
    }

    public void connect() throws InterruptedException {
        final Bootstrap bootstrap = new Bootstrap();
        final NettyClientHandler clientHandler = new NettyClientHandler();

        bootstrap.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(clientHandler);
            }
        });

        this.channel = bootstrap.connect(host, port).sync().channel();
    }

    public Channel getChannel() {
        return this.channel;
    }

    //-----
}
