package common;

/**
 * Created by Victor on 25.06.2017.
 */
public enum MessageState {
    NONE,
    SENT,
    ERROR
}